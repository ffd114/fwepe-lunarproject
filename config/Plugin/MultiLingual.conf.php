<?php

$cfg['DefaultLanguage']   = 'en';
$cfg['LanguageMethod']    = 'Uri'; //Choose between 'Uri' or 'Cookie'

//Only if language method is Uri;
$cfg['URIEngine']          = 'PathInfo';
$cfg['LanguageIdentifier'] = 'lang';

