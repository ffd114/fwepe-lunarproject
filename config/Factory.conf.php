<?php
$cfg['DefaultController'] = "Home";
$cfg['Environment']       = "production";
$cfg['ServerRequestUri']  = "PATH_INFO";

/**
 * Plugins
 *
 * Set comment to disable plugin
 * If the plugins are more than 1, put comma before new plugin
 */
$cfg['Plugins']           = array(
    /*
     * Multi lingual plugin to make system able to read language variable
     *
     * Priority should be : High if enabled
     *
     *
     */
    //new Fwepe\Factory\Plugin\MultiLingual()
);


/*** End: Factory.conf.php ***/
