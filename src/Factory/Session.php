<?php

namespace Fwepe\Factory;

use Fwepe\Component\Session\SessionManager;

class Session extends SessionManager
{

    public function __construct()
    {
        $this->config = new Config('Session');

        $options = array(
            'name'               => $this->config->get('Name'),
            'active_time'        => $this->config->get('ActiveTime'),
            'check_user_agent'   => $this->config->get('CheckUserAgent'),
            'refresh_session_id' => $this->config->get('RefreshSessionId'),
            'prefix'             => $this->config->get('Prefix'),
            'token'              => $this->config->get('Token')
        );

        parent::__construct($options);
    }


}
/*** End: Session.php ***/
