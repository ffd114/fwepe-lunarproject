<?php

namespace Fwepe\Factory;

use Fwepe\Component\Core\Kernel;
use Fwepe\Component\Plugin\PluginSubject;
use Fwepe\Component\Uri\Blueprint;

/**
 * The kernel
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Core
 */
class Factory extends PluginSubject
{

    public function __construct()
    {
        $this->config = new Config('Factory');

        $plugins = $this->config->get("Plugins");

        $this->loadPlugins($plugins);
        $this->triggerEvent('onConstruct');
    }
    
    
    /**
     * Handles URI Routing
     *
     */
    private function _setupKernel()
    {
        $key = $this->config->get('ServerRequestUri');
        
        $routes = config_get("Router", "Blueprint");
        
        $pathInfo = rtrim(filter_input(INPUT_SERVER, $key), "/");
        
        if(!$pathInfo)
            $pathInfo = '/';

        $blueprintFound = false;
        
        foreach($routes as $route)
        {
            $blueprint = new Blueprint($route[0], $route[1]);

            
            if($blueprint->match($pathInfo))
            {
                $tmp = $blueprint->getTarget();
                $class = $tmp['class'];
                $method = $tmp['method'];

                $params = $blueprint->getParams($pathInfo);
                $blueprintFound = true;
                break;
            }
        }
        
        if($blueprintFound)
        {

            $kernel = new Kernel($class, $method, $params);

            return $kernel->build();
        }
        else
        {
            //TODO: make better http 404 error
            throw new \Exception('Route error!');
        }
    }

    /**
     * This will setup Fwepe Environment
     *
     */
    private function _setupEnvironment()
    {
        $environment = $this->config->get('Environment');
        if($environment === 'development') {
            error_reporting(E_ALL | E_STRICT);
            ini_set('display_errors', true);
            ini_set('html_errors', true);
            ini_set('track_errors', true);
        }
    }

    /**
     * Build everything
     */
    protected function _build()
    {
        $this->_setupFunctions();
        $this->_setupEnvironment();
        $this->_setupKernel();
    }

    public function build()
    {
        $this->triggerEvent('beforeBuild');
        $this->_build();
        $this->triggerEvent('afterBuild');
    }

    private function _setupFunctions()
    {
        $systemFunction = SYSTEM_ROOT .DS. 'functions.php';
        if(is_file($systemFunction))
            require_once($systemFunction);

        $appFunction = APP_ROOT .DS. 'functions.php';
        if(is_file($appFunction))
            require_once($appFunction);
    }

}

/*** End: Factory.php ***/
