<?php

namespace Fwepe\Factory;

use Fwepe\Component\Security\AbstractSecurity;

class Security extends AbstractSecurity
{
    public function purify($text, $config = array())
    {
        if (is_array($text))
        {
            foreach ($text as $key => $val)
            {
                $text[$key] = $this->purify($val);
            }

            return $text;
        }

        if (trim($text) === '') {
            return $text;
        }

        $purifierConfig = \HTMLPurifier_Config::createDefault();

        $defaultConfig = config_get('Security', 'HtmlPurifier', array());
        if(!array_empty($defaultConfig))
            $config = array_merge($defaultConfig, $config);

        if(!array_empty($config));
            $purifierConfig->loadArray($config);
        $purifier = new \HTMLPurifier($purifierConfig);
        return $purifier->purify($text);
    }

}