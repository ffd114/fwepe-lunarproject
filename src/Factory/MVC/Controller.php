<?php

namespace Fwepe\Factory\MVC;

use Fwepe\Component\Plugin\PluginSubject;
use Fwepe\Factory\Config;

/**
 * Create controller
 *
 * @package Fwepe\Core
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 */

class Controller extends PluginSubject
{
    /**
     * To track loaded classes which will use as Controller's attribute
     *
     * @var array $loadedClass
     */
    static private $loadedClasses = array();

    /**
     * Controller's instance
     */
    static private $instance;

    /**
     * set $instance to $this
     */
    public function __construct()
    {
        $this->config = new Config("Controller");


        $plugins = $this->config->get("Plugins");

        $this->loadPlugins($plugins);
        $this->triggerEvent('onConstruct');

        $this->_setupObjects();

        self::$instance =& $this;
    }

    /**
     * Create attributes for each class defined in configuration
     */
    private function _setupObjects()
    {
        $objects = array();
        $autoload = array_merge($objects, $this->config->get("Autoload"));
        foreach($autoload as $key => $object) {
            $className = get_class($object);
            $className = trim($className, "\\");
            $className = explode("\\", $className);
            $className = array_pop($className);
            if(!in_array($className, self::$loadedClasses)) {
                self::$loadedClasses[] = $className;
                $className{0} = strtolower($className{0});
                if(is_int($key))
                    $controllerObject = $className;
                else
                    $controllerObject = $key;

                $this->$controllerObject = $object;
            }
        }
    }

    /**
     * To get the controller instance
     */
    static public function &getInstance()
    {
        return self::$instance;
    }
}

/*** End: Controller.php ***/
