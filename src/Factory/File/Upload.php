<?php

namespace Fwepe\Factory\File;

use Fwepe\Component\File\Upload as BaseUpload;
use Fwepe\Factory\Config;

class Upload extends BaseUpload
{

    public function __construct($file, $location, array $options = array())
    {
        $config = new Config('Upload');
        $this->options = array(
            'max_size'      => $config->get('MaxSize'),
            'permission'    => $config->get('Permission'),
            'location'      => $config->get('Location'),
            'pattern'       => $config->get('Pattern'),
            'counter_digit' => $config->get('CounterDigit'),
            'overwrite'     => $config->get('Overwrite')
        );

        parent::__construct($file, $location, $options);

        $this->setAllowedExtensions($config->get('AllowedExtensions'));
    }

}


/*** End: Upload.php ***/
