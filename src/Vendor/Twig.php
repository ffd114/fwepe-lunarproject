<?php

namespace Fwepe\Vendor;

use Fwepe\Factory\Config;

class Twig extends \Twig_Environment
{
    private $config;
    
    /**
     * 
     * TODO: Implement DI Container
     */
    public function __construct()
    {
        $this->config = new Config('Vendor/Twig');
        
        $loader = new \Twig_Loader_Filesystem(APP_ROOT .DS. 'templates');

        parent::__construct($loader, $this->config->get('TwigConfig'));

        $urlForFunction = new \Twig_SimpleFunction('url_for', function($name, $params = array()) {
            return url_for($name, $params);
        });
        $this->addFunction($urlForFunction);

    }
    
}
