<?php

namespace Fwepe\Helper;

//TODO: Create captcha function
class Captcha
{
    protected $folder = "FwepeCaptcha";
    protected $location = PATH_TMP;

    function deleteOldCaptcha()
    {
        $path = $this->location .DS. $this->folder;

        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                if ((time()-filectime($path ."/". $file)) > 300 && is_file($path ."/". $file)) {
                    unlink($path ."/". $file);
               }
            }
         }
    }

    private function _setupDir($path)
    {
        if(!is_dir($path)) {
            mkdir($path);
            chmod($path, 0777);
        }

    }

    function generate()
    {

        // generate random number and store in session
        $string = rand(1000, 9999);

        //generate image
        $im = imagecreatetruecolor(70, 25);

        //colors:
        $white = imagecolorallocate($im, 255, 255, 255);
        $grey = imagecolorallocate($im, 128, 128, 128);
        $black = imagecolorallocate($im, 0, 0, 0);

        imagefilledrectangle($im, 0, 0, 200, 35, $black);

        imagestring($im, 5, 22, 5, $string, $grey);
        imagestring($im, 5, 15, 6, $string, $white);


        for($i = 1; $i <= 3; $i++) {
            $x1 = rand(5, 65);
            $y1 = rand(5, 20);

            $x2 = rand(5, 65);
            $y2 = rand(5, 20);

            $randomColor = imagecolorallocate($im, rand(0, 255), rand(0,255), rand(0, 255));
            imageline($im, $x1, $y1, $x2, $y2, $randomColor);
        }

        $fileName = md5("FwepeCaptcha" . $string);
        $path = $this->location .DS. $this->folder;
        $this->_setupDir($path);
        $file =  $path .DS. $fileName . ".gif";
        imagegif($im, $file);
        imagedestroy($im);

        return array(
            "string" => $string,
            "file" => "{$this->folder}/$fileName.gif"
        );
    }
}

/*** End: Captcha.php ***/
