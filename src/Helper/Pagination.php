<?php

namespace Fwepe\Helper;

class Pagination
{
	const MODE_PREV_NEXT = 1;
	const MODE_FIRST_LAST = 2;

	private $mode = array();
	private $currentPage;
	private $className = "pagination";
	private $separator = " | ";
	private $page;
	private $prev = "<strong>Previous</strong>";
	private $next = "<strong>Next</strong>";
	private $first = "<strong>First</strong>";
	private $last = "<strong>Last</strong>";
    private $diff = 0;
    private $link;
    private $totalPage = 0;


	function __construct($link, $currentPage, $totalPage, $mode = array(Pagination::MODE_PREV_NEXT, Pagination::MODE_FIRST_LAST))
	{
		//$this->pages = $pages;
		if($currentPage < 1)
			$currentPage = 1;
		else if($currentPage > $totalPage)
			$currentPage = $totalPage;
			
		
		$this->currentPage = $currentPage;
		$this->link = $link;
		$this->totalPage = $totalPage;
		$this->mode = $mode;

	}

    function setDiff($diff)
    {
        $this->diff = $diff;
    }

    /**
     * Change language
     * @param string $prev
     * @param string $next
     */
	function setPrevNext($prev, $next)
	{
		$this->prev = $prev;
		$this->next = $next;
	}
	
	public function setFirstLast($first, $last)
	{
		$this->first = $first;
		$this->last = $last;
	}

        /**
         * Change separator for each page
         * @param string $new_separator
         */
	function setSeparator($newSeparator)
	{
		$this->separator = $newSeparator;
	}

        /**
         * Set tag <div></div> class to use on template
         * @param string $class_name
         */
	function setClassName($className)
	{
		$this->className = $className;
	}

        /**
         * Show pages
         * @return string
         */
	function output()
	{
		$string = '';

		$pages = array();

        $minRange = $this->currentPage - $this->diff;
        $maxRange = $this->currentPage + $this->diff;
        $before = false;
		for($page = 1; $page <= $this->totalPage; $page++)
        {
			$link = str_replace("(*)", $page, $this->link);
			
            $index = $page;   
            
            if($this->diff == 0)
            {
                if($this->currentPage == $page)
                {
                    $temp = "<label>" . ($index) . "</label>";
                }
                else
                {
                    $temp = '<a href="' .$link. '">' . ($index) . '</a>';
                }

                $temp = "<span>" . $temp . "</span>";
                $pages[] = $temp;
            }
            else if($this->diff > 0)
            {
                if($index < $minRange & $before == false)
                {
                    $pages[] = "<span>...</span>";
                    $before = true;
                }
                else if ($index >= $minRange && $index <= $maxRange)
                {
                    if($this->currentPage == $page)
                    {
                        $temp = "<label>" . ($index) . "</label>";
                    }
                    else
                    {
                        $temp = '<a href="' .$link. '">' . ($index) . '</a>';
                    }

                    $temp = "<span>" . $temp . "</span>";
                    $pages[] = $temp;
                }
                else if($index > $maxRange)
                {
                    $pages[] = "<span>...</span>";
                    break;
                }


            }
		}

        $string = implode($this->separator, $pages);


		if(in_array(self::MODE_PREV_NEXT, $this->mode))
        {

            $prevPage = $this->currentPage - 1;
            if($prevPage > 0)
            {
				$prevLink = str_replace("(*)", $prevPage, $this->link);
                $this->prev = '<a href="' . $prevLink. '">' .$this->prev. '</a>';
            }
            else
            {
                $this->prev = "<label>" . $this->prev . "</label>";
            }

            $nextPage = $this->currentPage + 1;
            if ($nextPage <= $this->totalPage)
            {
				$nextLink = str_replace("(*)", $nextPage, $this->link);
                $this->next = '<a href="' .  $nextLink . '">' .$this->next. '</a>';
            }
            else
            {
                $this->next = "<label>" . $this->next . "</label>";
            }

            $string = $this->prev . $this->separator . $string . $this->separator . $this->next;

		}
		
		if(in_array(self::MODE_FIRST_LAST, $this->mode))
        {
            if($this->currentPage > 1)
            {
				$firstLink = str_replace("(*)", 1, $this->link);
                $this->prev = '<a href="' . $firstLink. '">' .$this->first. '</a>';
            }
            else
            {
                $this->prev = "<label>" . $this->first . "</label>";
            }
            
            if ($this->currentPage < $this->totalPage)
            {
				$lastLink = str_replace("(*)", $this->totalPage, $this->link);
                $this->next = '<a href="' .  $lastLink . '">' .$this->last. '</a>';
            }
            else
            {
                $this->next = "<label>" . $this->last . "</label>";
            }

            $string = $this->prev . $this->separator . $string . $this->separator . $this->next;

		}

		if(!empty($this->className))
        {
			$string = '<div class="' . $this->className . '">' . $string .
					  '</div>';
		}
		
		return $string;
	}

}

/* End: Pagination.php */
