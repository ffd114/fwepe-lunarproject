<?php

namespace Fwepe\Component\Uri\Builder;

abstract class AbstractUriBuilder
{
    protected $originalUri = '';

    protected $fullUri = '';

    protected $newUri = '';

    protected $data = array();

    //abstract public function __construct();

    public function getOriginalUri()
    {

        return $this->originalUri;

    }

    abstract public function clean();

    abstract public function build($addUri);

    public function fetch()
    {
        return $this->newUri;

    }

    public function setValue($name, $value = '')
    {
        $this->data[$name] = $value;
    }

    public function add($name, $value = '')
    {
        if(!array_key_exists($name, $this->data))
        {
            $this->data[$name] = $value;
        }
    }


    public function getValue($name)
    {
        if(isset($this->data[$name]))
            return $this->data[$name];
        else
            return false;

    }
}

/*** End: AbstractUriBuilder.php ***/
