<?php

namespace Fwepe\Component\Uri\Builder;

class PathInfoBuilder extends AbstractUriBuilder
{   
    public function __construct($serverKey = 'PATH_INFO')
    {
        $value = filter_input(INPUT_SERVER, $serverKey);
        
        if(!empty($value))
            $this->fullUri = trim($value, '/');
    }

    public function clean()
    {
        $tmpUri = array();

        if(!empty($this->fullUri))
            $tmpUri = explode('/', $this->fullUri);

        $i = 0;

        foreach($this->data as $param => $data) {
            if(isset($tmpUri[$i]))
            {
                $this->setValue($param, $tmpUri[$i]);
                unset($tmpUri[$i]);
            }
            $i++;
        }

        $this->originalUri = implode('/', $tmpUri);
    }

    public function build($addUri)
    {
        $addUri = explode('/', trim($addUri, '/'));
        
        $newUri = array_merge($this->data, $addUri);

        $this->newUri  = implode('/', $newUri);
    }


}

/*** End: PathInfoBuilder.php ***/
