<?php

namespace Fwepe\Component\Uri;

class Dispatcher
{
    private static $uriBuilder;

    public static function isRegistered($builder)
    {
        return isset(self::$uriBuilder[$builder]);
    }

    public static function register($builder, $name, $value = '')
    {
        if(!self::isRegistered($builder)) {
            $builderClass = '\Fwepe\Component\Uri\Builder\\' . $builder . 'Builder';
            self::$uriBuilder[$builder] = new $builderClass();
        }

        self::$uriBuilder[$builder]->add($name, $value);
        self::$uriBuilder[$builder]->clean();
    }

    public static function set($builder, $name, $value)
    {
        self::$uriBuilder[$builder]->setValue($name, $value);
        self::$uriBuilder[$builder]->clean();
    }

    public static function getOriginalUri($builder)
    {
        return self::$uriBuilder[$builder]->getOriginalUri();
    }

    public static function fetch($builder, $addUri)
    {
        self::$uriBuilder[$builder]->build($addUri);
        return self::$uriBuilder[$builder]->fetch();
    }

    public static function get($builder, $name)
    {
        return self::$uriBuilder[$builder]->getValue($name);
    }

}

/*** End: Dispatcher.php ***/
