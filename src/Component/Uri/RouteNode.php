<?php

namespace Fwepe\Component\Uri;

/**
 * RouteNode for using with Blueprint class
 *
 * @author Farly FD <farly[at]lunarproject[dot]org>
 * @package Fwepe\Component\Uri
 */
class RouteNode
{
    protected $_index;
    protected $_name = null;
    protected $_value = null;
    
    public function setIndex($index)
    {
        $this->_index = $index;
    }
    
    public function getIndex()
    {
        return $this->_index;
    }
    
    public function getName()
    {
        return $this->_name;
    }
    
    public function setName($name)
    {
        $this->_name = $name;
    }
    
    public function setValue($value)
    {
        $this->_value = $value;
    }
    
    public function getValue()
    {
        return $this->_value;
    }
    
    public function toString()
    {
        $str = "{";
        $str .= $this->getIndex();
        
        if($this->getName())
            $str .= ":" . $this->getName();
        
        $str .= "}";
        
        return $str;
    }
}
