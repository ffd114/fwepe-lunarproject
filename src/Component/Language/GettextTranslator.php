<?php

namespace Fwepe\Component\Language;

class GettextTranslator extends AbstractTranslator
{
    public function __construct()
    {
        if(!extension_loaded('gettext'))
            trigger_error('PHP Module \'gettext\' not loaded', E_USER_ERROR);

        $domain = MY_APP;
        $path   = PATH_APP .DS. MY_APP .DS. 'Locale';

        bindtextdomain($domain, $path);
        textdomain($domain);
    }

    public function setLanguage($locale)
    {
        if(setlocale(LC_ALL, $locale))
            return true;
        else
            return false;
    }

    public function translate($text)
    {
        return gettext($text);
    }
}


/*** End: GettextTranslator.php ***/
