<?php
namespace Fwepe\Component\Plugin;

class PluginSubject
{
    /**
     * Plugins
     *
     * @var array $plugins
     */
    protected $plugins = array();

    /**
     * Trigger event at plugin
     *
     * @param string $eventHandler
     */
    public function triggerEvent($eventHandler)
    {
        $args = func_num_args();
        if($args > 1) {

            $obj = $args[1];
            $event = $args[0];
            if (method_exists($obj, $event))
                call_user_func(array($obj, $event));
        } else {
            foreach($this->plugins as $obj) {
                if(method_exists($obj, $eventHandler))
                    call_user_func(array($obj, $eventHandler));
            }
        }

    }

    /**
     *
     * Load plugins
     *
     * @param array $plugins
     */
    public function loadPlugins(array $plugins)
    {
        foreach($plugins as $plugin) {
            if($plugin instanceof BasePlugin)
                $this->loadPlugin($plugin);
            else
                trigger_error("Loaded plugin is not an instance of BasePlugin", E_USER_ERROR);
        }

    }

    /**
     *
     * Load plugin
     *
     * @param BasePlugin $plugin
     */
    public function loadPlugin(BasePlugin &$plugin)
    {
        $this->plugins[$plugin->id] =& $plugin;
        $plugin->loadSubject($this);
        $this->triggerEvent('onLoad', $plugin);

    }

    /**
     * Remove plugin
     *
     * @param string $id
     */

    public function removePlugin($id)
    {
        $this->triggerEvent('onUnload', $this->plugins[$id]);
        unset($this->plugins[$id]);
    }

}
