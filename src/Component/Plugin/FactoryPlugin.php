<?php


namespace Fwepe\Component\Plugin;

abstract class FactoryPlugin extends BasePlugin
{
    /**
     * When Factory is constructed
     */
    abstract public function onConstruct();

    /**
     * Before Factory builds the application
     */
    abstract public function beforeBuild();

    /**
     * After Factory builds the application
     */
    abstract public function afterBuild();

}

/*** End: KernelPlugin.php ***/
