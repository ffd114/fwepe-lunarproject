<?php

namespace Fwepe\Component\Plugin;

abstract class BasePlugin
{
    /**
     * Identifier. Should be set for every plugin created
     */
    public $id;

    /**
     * Load the subject
     *
     * @param Object $subject
     */
    public function loadSubject(&$subject)
    {
        $this->subject =& $subject;
    }



}

