<?php

namespace Fwepe\Component\File;

class ImageUpload extends Upload
{
    protected $mimeType;

    protected $allowedExtensions = array(
        'image/jpeg',
        'image/jpeg',
        'image/png',
        'image/gif'
    );

    public function getMimeType()
    {
        if(empty($this->mimeType)) {
            $image = getimagesize($this->file['tmp_name']);
            if ($image !== false)
                $this->mimeType =  $image['mime'];
            else
                $this->mimeType = false;
        }

        return $this->mimeType;
    }

    public function getExtension()
    {
        $mimeType = $this->getMimeType();
        if($mimeType) {
            $mimeType = explode('/', $mimeType);
            $extension = end($mimeType);
            return $extension;
        } else {
            return parent::getExtension();
        }
    }

    /**
     * Prepare the file to be uploaded
     *
     * @return boolean
     */
    public function prepare()
    {
        extract($this->options);

        if ($this->file['error'] > 0 && $this->file['error'] < 10)
        {
            $this->status['error'] = $this->file['error'];
            switch($this->file['error']) {
                case 1:
                    $this->status['message'] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case 2:
                    $this->status['message'] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case 3:
                    $this->status['message'] = 'The uploaded file was only partially uploaded';
                    break;
                case 4:
                    $this->status['message'] = 'No file was uploaded';
                    break;
                case 6:
                    $this->status['message'] = 'Missing a temporary folder';
                    break;
                case 7:
                    $this->status['message'] = 'Failed to write file to disk';
                    break;
                case 8:
                    $this->status['message'] = 'A PHP extension stopped the file upload';
                    break;
            }
            return false;
        }

        if (!in_array($this->getMimeType(), $this->allowedExtensions)) {
            $this->status['error'] = 10;
            $this->status['message'] = 'File extension is not allowed!';
            return false;
        }

        if($this->file['size'] > $max_size) {
            $this->status['error'] = 11;
            $this->status['message'] = sprintf('The uploaded file exceeds max file size: %d Byte', $max_size);
            return false;
        }

        $this->location = rtrim($this->location, '/');
        $this->location = rtrim($this->location, '\\');
        $this->location = rtrim($this->location, DS);
        if(!is_dir($this->location)) {
            $locationStatus = mkdir($this->location, $permission, true);
            chmod($this->location, $permission);
            if($locationStatus === false) {
                $this->status['error'] = 12;
                $this->status['message'] = 'Cannot reach upload location. Cannot move file';
                return false;
            }
        }

        if(!is_writable($this->location)) {
            $this->status['error'] = 13;
            $this->status['message'] = 'Uploaded file location is not writable. Cannot move file';
            return false;
        }

        if(!is_uploaded_file($this->file['tmp_name'])) {
            $this->status['error'] = 14;
            $this->status['message'] = 'File not uploaded via HTTP. Possible file upload attack';
            return false;
        }

        if(!is_file($this->location .DS. 'index.html')) {
            fclose(fopen($this->location .DS. 'index.html', 'w'));
            @chmod($this->location .DS. 'index.html', $permission);
        }

        $this->status['upload'] = true;
        $this->status['error']  = 0;
        $this->status['message'] = 'File is ready to upload';
        return true;
    }

}

/*** End: ImageUpload.php ***/
