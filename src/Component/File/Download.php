<?php

namespace Fwepe\Component\File;

/**
 * Download without having to show direct file location
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Component\File
 */
class Download extends FileInfo implements Executable
{
    /**
     * Download status
     *
     * @var array
     */
    public $status = array(
        'download' => false,
        'error'    => -1,
        'message'  => ''
    );

    /**
     * Prepare downloaded file. Checking if all requirement is qualified
     *
     * @return boolean
     */
    public function prepare()
    {
        if(!is_dir($this->location)) {
            $this->status['error']   = 1;
            $this->status['message'] = 'Download location not found';
            return false;
        }

        if(!is_file($this->location .DS. $this->file)) {
            $this->status['error']   = 2;
            $this->status['message'] = 'Download file not found';
            return false;
        }

        if(!is_readable($this->location .DS. $this->file)) {
            $this->status['error']   = 3;
            $this->status['message'] = 'Cannot read downloaded file';
            return false;
        }

        $this->status['download'] = true;
        $this->status['error']    = 0;
        $this->status['message']  = 'File is ready to be downloaded';
        return true;
    }

    /**
     * Check if download is ready
     *
     * @return boolean
     */
    public function isReady()
    {
        return $this->status['download'];
    }

    /**
     * Download the file if everything is ready
     */
    public function execute()
    {
        if(!$this->isReady()) {
            trigger_error('File is not ready to download', E_USER_WARNING);
            return false;
        }

        set_time_limit(0);

        header('Content-Description: File Transfer');
		header('Content-Type: ' . $this->getMimeType());
		header('Content-Disposition: attachment; filename="' . $this->getFile() . '"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . $this->getSize());
        
		ob_clean();
		flush();
		//readfile($this->location .DS. $this->file);

        $this->chunkIt($this->location .DS. $this->file);
		//ob_clean();
		//flush();
		//echo file_get_contents($this->location .DS. $this->file);
		exit;
    }

    public function chunkIt($filename,$retbytes=true) {
       $chunksize = 1*(1024*1024); // how many bytes per chunk
       $buffer = '';
       $cnt =0;
       // $handle = fopen($filename, 'rb');
       $handle = fopen($filename, 'rb');
       if ($handle === false) {
           return false;
       }
       while (!feof($handle)) {
           $buffer = fread($handle, $chunksize);
           echo $buffer;
           ob_flush();
           flush();
           if ($retbytes) {
               $cnt += strlen($buffer);
           }
       }
           $status = fclose($handle);
       if ($retbytes && $status) {
           return $cnt; // return num. bytes delivered like readfile() does.
       }
       return $status;

    }

}

/*** End: Download.php ***/
