<?php

namespace Fwepe\Exception;

/**
 * Description of RouterException
 *
 * @author Farly FD <farly[dot]fd[at]gmail[dot]com>
 * @package Fwepe\Exception
 */
class RouterException extends \Exception {}
